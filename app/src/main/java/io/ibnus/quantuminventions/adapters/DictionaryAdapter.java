package io.ibnus.quantuminventions.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.ibnus.quantuminventions.Model.Dictionary;
import io.ibnus.quantuminventions.R;



class DictionaryViewHolder extends RecyclerView.ViewHolder {

    TextView user_word,frequency;
    LinearLayout linear_lay;


    DictionaryViewHolder(View itemView)
    {
        super(itemView);
        user_word = itemView.findViewById(R.id.text_word);
        frequency = itemView.findViewById(R.id.text_freq);
        linear_lay = itemView.findViewById(R.id.linear_lay);


    }



}


public class DictionaryAdapter extends RecyclerView.Adapter<DictionaryViewHolder>
{

    private List<Dictionary> listData = new ArrayList<>();
    private Context context;
    DictionaryAdapter adapter;


    public DictionaryAdapter(List<Dictionary> listData, Context context) {
        this.listData = listData;
        this.context = context;

    }

    @NonNull
    @Override
    public DictionaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemview = inflater.inflate(R.layout.dictionary_layout,parent,false);

        return new DictionaryViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull final DictionaryViewHolder holder, int position)
    {

       final Dictionary current = listData.get(position);

        holder.user_word.setText(current.getWord());
        holder.frequency.setText(String.valueOf(current.getFrequency()));

        if (current.isSelected())
        {
            //Highlighting the word spoken by user
            holder.linear_lay.setBackground(ContextCompat.getDrawable(context,R.drawable.my_bg));
        }
        else
        {

            holder.linear_lay.setBackground(ContextCompat.getDrawable(context,R.drawable.my_bg_default));
        }







    }
    @Override
    public int getItemCount() {
        return listData.size();
    }
}
