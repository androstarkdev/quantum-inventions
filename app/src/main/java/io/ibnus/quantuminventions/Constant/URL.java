package io.ibnus.quantuminventions.Constant;

public class URL {
    private static final String DOMAIN_URL = "http://a.galactio.com/";


    public static final String DICTIONARY_URL = DOMAIN_URL+"interview/dictionary-v2.json";

    public static final String SPEECH_TO_TEXT = "https://speech.googleapis.com/v1/speech:recognize?key=";

    public static final String TEXT_TO_SPEECH = "https://texttospeech.googleapis.com/v1/text:synthesize?fields=audioContent&key=";


}
