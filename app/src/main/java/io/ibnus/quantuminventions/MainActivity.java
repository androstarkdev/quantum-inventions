package io.ibnus.quantuminventions;

import androidx.annotation.NonNull;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.valdesekamdem.library.mdtoast.MDToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.ibnus.quantuminventions.Common.Common;
import io.ibnus.quantuminventions.Constant.URL;
import io.ibnus.quantuminventions.Model.Dictionary;

import io.ibnus.quantuminventions.Util.EnglishNumberToWords;
import io.ibnus.quantuminventions.Util.MyComparator;
import io.ibnus.quantuminventions.adapters.DictionaryAdapter;

public class MainActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    DictionaryAdapter mAdapter;

    private Button recordBtn;
    private TextView user_status;

    boolean playing = false;

    private static final int RECORDER_BPP = 16;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = "temp_audio.wav";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private AudioRecord recorder = null;
    private int bufferSize = 0;
    private Thread recordingThread = null;
    private boolean isRecording = false;
    MediaPlayer mp;
    public  static final int PERMISSIONS_MULTIPLE_REQUEST = 123;



    Map<String,Integer> words_list = new HashMap<>();

    List<Dictionary> listdata = new ArrayList<>();

    boolean autoBotPlay = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initializing views
        this.initializeViews();
        
        
        if (Common.isConnectedToInternet(this)) {
              
            
                //Requesting User For Permissions
                checkPermission();
                
                //Fetching Data from API
                getDictionaryFromApi();
                
        } else {
            try {
                AlertDialog alert = new AlertDialog.Builder(this).create();
                alert.setTitle("No Internet");
                alert.setMessage("Please turn on internet connection to use the App !");
                alert.setButton(Dialog.BUTTON_POSITIVE,"OK",new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        
                        //closing the APP and redirecting to Device Wireless Settings Page 
                        startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                        finish();
                    }
                });

                alert.show();

            } catch (Exception e) {
                    e.printStackTrace();
            }
        }

        bufferSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_MONO,AudioFormat.ENCODING_PCM_16BIT);
        
        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                {

                    if (!playing)
                    {
                        recordBtn.setText(getString(R.string.stop));
                        recordBtn.setCompoundDrawablesWithIntrinsicBounds(null,null,ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_stop),null);

                        playing = true;
                        user_status.setText(getString(R.string.user_talking));

                        //Calling Function to Start Voice Recording
                        startRecording();
                    }
                    else
                    {
                        user_status.setText(getString(R.string.bot_reply));
                        recordBtn.setText(getString(R.string.start));

                        recordBtn.setCompoundDrawablesWithIntrinsicBounds(null,null,ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_microphone),null);
                        playing = false;

                        //Calling Function to Stop Voice Recording
                        stopRecording();
                    }

                }
                else
                {
                    checkPermission();
                }

            }
        });



       
    }

    private void initializeViews() {
        recyclerView = findViewById(R.id.dictionary_list);
        user_status= findViewById(R.id.user_bot_text);
        recordBtn = findViewById(R.id.recording_btn);



        mAdapter = new DictionaryAdapter(listdata,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(mAdapter);

    }

    private void getDictionaryFromApi() {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL.DICTIONARY_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                
                try {



                    JSONObject jobj = new JSONObject(response);
                   JSONArray jsonArray = jobj.getJSONArray("dictionary");

                        for(int i=0;i<jsonArray.length();i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Dictionary dictionaryData = new Dictionary();
                            dictionaryData.setWord(jsonObject.getString("word").toLowerCase());
                            dictionaryData.setFrequency(jsonObject.getInt("frequency"));

                            listdata.add(dictionaryData);

                        }



                        //Sorting Frequencies in Descending Order in the List using Custom Comparator 
                        Collections.sort(listdata,new MyComparator());

                        //Let adapter refresh with the current list 
                        mAdapter.notifyDataSetChanged();


                    }

                catch (JSONException e)
                {

                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(),
                                    "Time Out! Try Again", Toast.LENGTH_LONG).show();

                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(MainActivity.this,"Oops!!! Authentication Failure", Toast.LENGTH_LONG).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(MainActivity.this,"Oops!!! Server Error", Toast.LENGTH_LONG).show();
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(MainActivity.this,"Oops!!! Network Error", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }



    private void playDefaultBotAudio() {




        autoBotPlay = true;

        user_status.setText(getString(R.string.bot_talk));
        recordBtn.setVisibility(View.GONE);

        convertToAudio(getString(R.string.welcome_audio));
    }


    private void convertToText(final String encoded_audio) {

        for (int i=0;i<listdata.size();i++)
        {
            Dictionary dict = new Dictionary();
            dict.setSelected(false);
            dict.setWord(listdata.get(i).getWord());
            dict.setFrequency(listdata.get(i).getFrequency());

            words_list.put(listdata.get(i).getWord().toLowerCase(),i);
            listdata.set(i,dict);
        }


        recordBtn.setVisibility(View.GONE);

        
        //Using Volly Library to Fetch Data from API
        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, URL.SPEECH_TO_TEXT+getString(R.string.google_api_key), new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                try
                {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("results");

                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    JSONArray jsonArray2 = jsonObject1.getJSONArray("alternatives");

                    JSONObject jsonObject2 = jsonArray2.getJSONObject(0);
                    
                    String user_reply = jsonObject2.getString("transcript").toLowerCase();


                    //If word is a number convert the number to word so that it can match with dictionary words
                    if (Common.isInteger(user_reply))
                    {
                          user_reply =   EnglishNumberToWords.convert(Integer.parseInt(user_reply));
                    }

               
                        if (words_list.containsKey(user_reply))
                        {

                            if (words_list.get(user_reply)!=null)
                            {
                                //Ignore this warning as this value is self defined position
                                int pos =words_list.get(user_reply);

                                convertToAudio(getString(R.string.match_found));

                                
                                Dictionary dictionary = new Dictionary();


                                dictionary.setSelected(true);
                                dictionary.setFrequency(listdata.get(pos).getFrequency()+1);
                                dictionary.setWord(listdata.get(pos).getWord());



                                listdata.set(pos,dictionary);
                                
                                //Sorting List again using Comparator after updated frequencies in the List
                                Collections.sort(listdata,new MyComparator());
                                mAdapter.notifyDataSetChanged();
                                MDToast.makeText(MainActivity.this, "Match Found", Toast.LENGTH_SHORT, MDToast.TYPE_SUCCESS).show();

                            }


                        }
                        else
                        {

                            //If word not found then Initializing a default bot audio and refreshing the Adapter
                            mAdapter.notifyDataSetChanged();
                            convertToAudio(getString(R.string.no_match));
                            MDToast.makeText(MainActivity.this, "No Match Found!", Toast.LENGTH_SHORT, MDToast.TYPE_INFO).show();

                        }


                } catch (JSONException e) {
                    e.printStackTrace();
                    //Possible exception when the response is empty that means words wasn't recognized by Google API and thus handling the exception with default Bot Audio
                    mAdapter.notifyDataSetChanged();
                    convertToAudio(getString(R.string.no_match));
                    MDToast.makeText(MainActivity.this, "No Match Found!", Toast.LENGTH_SHORT, MDToast.TYPE_INFO).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {

                user_status.setText(getString(R.string.speak_word));
                recordBtn.setVisibility(View.VISIBLE);
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getApplicationContext(),
                            "Time Out! Try Again", Toast.LENGTH_LONG).show();

                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(MainActivity.this,"Oops!!! Authentication Failure", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(MainActivity.this,"Oops!!! Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(MainActivity.this,"Oops!!! Network Error", Toast.LENGTH_LONG).show();
                }


            }
        }) {




            @Override
            public byte[] getBody() throws AuthFailureError {

                JSONObject mainObject = new JSONObject();


                try {

                    JSONObject childObject1 = new JSONObject();
                    childObject1.put("languageCode",Locale.ENGLISH);
                    childObject1.put("enableWordTimeOffsets",false);
                    childObject1.put("audio_channel_count",1);

                    JSONObject childObject2 = new JSONObject();
                    childObject2.put("content",encoded_audio);

                    mainObject.put("config",childObject1);
                    mainObject.put("audio",childObject2);


                } catch (JSONException e) {
                    e.printStackTrace();
                }




                return mainObject.toString().getBytes();
            }

            public String getBodyContentType()
            {
                return "application/json; charset=utf-8";
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(MyStringRequest);
    }

    private void convertToAudio(final String translated_text) {

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, URL.TEXT_TO_SPEECH+getString(R.string.google_api_key), new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {


                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    writeToFile(jsonObject.getString("audioContent"));

                    File file = new File(Environment.getExternalStorageDirectory() + "/testing.mp3");

                    //Decoding Base64 to Audio File
                    decodeAudio(jsonObject.getString("audioContent"),file,Environment.getExternalStorageDirectory() + "/testing.mp3");

                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getApplicationContext(),
                            "Time Out! Try Again", Toast.LENGTH_LONG).show();

                } else if (error instanceof AuthFailureError) {
                    Toast.makeText(MainActivity.this,"Oops!!! Authentication Failure", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Toast.makeText(MainActivity.this,"Oops!!! Server Error", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Toast.makeText(MainActivity.this,"Oops!!! Network Error", Toast.LENGTH_LONG).show();
                }
            }
        }) {




            @Override
            public byte[] getBody() throws AuthFailureError {

                JSONObject mainObject = new JSONObject();


                try {

                    JSONObject childObject1 = new JSONObject();
                    childObject1.put("ssml",translated_text);

                    JSONObject childObject2 = new JSONObject();
                    childObject2.put("languageCode", Locale.ENGLISH);
                    childObject2.put("ssmlGender","FEMALE");

                    JSONObject childObject3 = new JSONObject();
                    childObject3.put("audioEncoding","MP3");



                    mainObject.put("audioConfig",childObject3);
                    mainObject.put("input",childObject1);
                    mainObject.put("voice",childObject2);

                } catch (JSONException e) {
                    e.printStackTrace();
                }




                return mainObject.toString().getBytes();
            }

            public String getBodyContentType()
            {
                return "application/json; charset=utf-8";
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(MyStringRequest);
    }


    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)+
                ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.RECORD_AUDIO)) {

                Snackbar.make(MainActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to use application",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},
                        PERMISSIONS_MULTIPLE_REQUEST);


            }
        } else {
            if (!autoBotPlay)
            {
                playDefaultBotAudio();
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSIONS_MULTIPLE_REQUEST) {
            if (grantResults.length > 0) {
                boolean recordingPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean writeExternalFile = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (recordingPermission && readExternalFile && writeExternalFile) {

                    if (!autoBotPlay) {
                        playDefaultBotAudio();
                    }

                } else {
                    Snackbar.make(MainActivity.this.findViewById(android.R.id.content),
                            "Please Grant All Permissions to use the App",
                            Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    requestPermissions(
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},
                                            PERMISSIONS_MULTIPLE_REQUEST);
                                }
                            }).show();
                }
            }
        }
    }





    private String getFilename(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if(!file.exists()){
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_FILE_EXT_WAV);
    }

    private String getTempFilename(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if(!file.exists()){
            file.mkdirs();
        }

        File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);

        if(tempFile.exists())
            tempFile.delete();

        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

    private String getTempFilename2(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if(!file.exists()){
            file.mkdirs();
        }

        File tempFile = new File(filepath,AUDIO_RECORDER_FILE_EXT_WAV);


        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_FILE_EXT_WAV);
    }



    private void startRecording(){


        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING, bufferSize);

        int i = recorder.getState();
        if(i==1)

        recorder.startRecording();

        isRecording = true;

        recordingThread = new Thread(new Runnable() {

            @Override
            public void run() {

                writeAudioDataToFile();
            }
        },"AudioRecorder Thread");

        recordingThread.start();
    }

    private void writeAudioDataToFile(){
        byte data[] = new byte[bufferSize];


        String filename = getTempFilename();
        FileOutputStream os = null;

        try {
            os = new FileOutputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int read = 0;

        if(null != os){
            while(isRecording){
                read = recorder.read(data, 0, bufferSize);

                if(AudioRecord.ERROR_INVALID_OPERATION != read){
                    try {
                        os.write(data);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void stopRecording(){

        if(null != recorder){
            isRecording = false;

            int i = recorder.getState();
            if(i==1)


                recorder.stop();
            recorder.release();

            recorder = null;
            recordingThread = null;
        }

        //Converting Audio FIle to WAV format
        copyWaveFile(getTempFilename(),getFilename());

        File file = new File(getTempFilename2());

        //Encoding Audio to Base64
        String encode_audio = getStringFile(file);

        writeToFile(encode_audio);


        try{

            //Calling Function for Google Speak to Text API
            convertToText(encode_audio);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }


    private void copyWaveFile(String inFilename,String outFilename){

        FileInputStream in = null;
        FileOutputStream out = null;
        long totalAudioLen = 0;
        long totalDataLen = totalAudioLen + 36;
        long longSampleRate = RECORDER_SAMPLERATE;
        int channels = 1;
        long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels/8;

        byte[] data = new byte[bufferSize];

        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;

            WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
                    longSampleRate, channels, byteRate,outFilename);

            while(in.read(data) != -1){
                out.write(data);

            }

            in.close();
            out.close();



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void WriteWaveFileHeader(
            FileOutputStream out, long totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate, String outFilename) throws IOException {

        byte[] header = new byte[44];

        header[0] = 'R';  // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';  // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1;  // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8);  // block align
        header[33] = 0;
        header[34] = RECORDER_BPP;  // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

        out.write(header, 0, 44);



    }



    public String getStringFile(File f) {
        InputStream inputStream = null;
        String encodedFile= "", lastVal;
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }


            output64.close();


            encodedFile =  output.toString();

        }
        catch (FileNotFoundException e1 ) {
            e1.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal.replaceAll("\\n","");
    }

    private void writeToFile(String content) {




        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/test.txt");

            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter writer = new FileWriter(file);
            writer.append(content);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    private void decodeAudio(String base64AudioData, File fileName, String path) {

        try {

            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(Base64.decode(base64AudioData.getBytes(), Base64.DEFAULT));
            fos.close();
            user_status.setText(getString(R.string.bot_talk));
            try {

                mp  = new MediaPlayer();
                mp.setDataSource(path);

                mp.prepare();
                mp.start();

                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        recordBtn.setVisibility(View.VISIBLE);
                        user_status.setText(getString(R.string.speak_word));
                    }
                });

            } catch (Exception e) {

                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mp!=null)
        {
            mp.stop();
            mp.release();
        }

    }




}
