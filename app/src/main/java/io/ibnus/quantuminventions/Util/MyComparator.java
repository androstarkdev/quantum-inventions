package io.ibnus.quantuminventions.Util;
import java.util.Comparator;

import io.ibnus.quantuminventions.Model.Dictionary;


public class MyComparator implements Comparator<Dictionary>
{
    @Override
    public int compare(Dictionary a, Dictionary b) {
        return Integer.compare(b.getFrequency(), a.getFrequency());
    }
}