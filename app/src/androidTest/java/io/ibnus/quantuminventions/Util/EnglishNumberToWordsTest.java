package io.ibnus.quantuminventions.Util;

import org.junit.Assert;
import org.junit.Test;

import io.ibnus.quantuminventions.Common.Common;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class EnglishNumberToWordsTest {

    @Test
    public void convert() {
        int testnum = 20;
        Assert.assertThat(EnglishNumberToWords.convert(testnum), is("twenty"));

    }
}